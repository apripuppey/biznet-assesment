
function formatting(array){
	let data = {
		h : [],
		d : []
	};

	data.h = Object.keys(array[0]);

	array.forEach( function (item){
		data.d.push(Object.values(item));
	})

	return JSON.stringify(data);

}

var data=[{"username":"ali","hair_color":"brown","height":1.2},{"username":"marc","hair_color":"blue","height":1.4},{"username":"joe","hair_color":"brown","height":1.7},{"username":"zehua","hair_color":"black","height":1.8}];

console.log(formatting(data));